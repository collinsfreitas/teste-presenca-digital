export interface IProdutos {
    id: number,
    tipo: string,
    nome: string,
    descricao: string,
    exibir: number,
    ordem: number,
    adesao: number,
    taxaInstalacao: number,
    canais: number
}
