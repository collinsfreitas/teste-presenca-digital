import { Component, OnInit } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { registerLocaleData } from '@angular/common';
import pt from '@angular/common/locales/pt';

interface DataResponse {
  id: number,
  tipo: string,
  nome: string,
  descricao: string,
  exibir: number,
  ordem: number,
  adesao: number,
  taxaInstalacao: number,
  canais: number,
  listaCanais: null,
  preco: number,
  precoDe: number
}

@Component({
  selector: 'app-produtos',
  templateUrl: './produtos.component.html',
  styleUrls: ['./produtos.component.scss']
})
export class ProdutosComponent implements OnInit {

  constructor( private http: HttpClient ) {}
  
  arrayProdutos: any = [];
  



  ngOnInit(): void {

    registerLocaleData(pt);

    var tempArray: DataResponse[] = [];
    this.http.get('/assets/data/produtos.json').subscribe(
      data => {
                this.arrayProdutos = tempArray;              
                for (var i=0; i<10; i++)  
                  tempArray.push(<DataResponse>data[i]);     
              },
      error=> { 
                console.log("Erro ao carregar dados."); 
              },
      ()   => {
                console.log( tempArray[0] );
              }
    );
  }

}
